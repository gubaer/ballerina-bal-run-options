import ballerina/io;

public function main(string... args) returns error? {
    var len = args.length();
    if args.length() == 0 {
        io:println("No command line options passed in");
        return;
    }
    io:println(string`number of command line options passed in:  ${len}`);
    foreach var entry in args.enumerate() {
        io:println(string`arg [${entry[0]}]: '${entry[1]}'`);
    }
}